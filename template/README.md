# $course 

These are the lecture notes for the '$course', taught in $term at the University of Bonn.

The [latest version][1] is availabe as a pdf download via GitLab runner.
You can also have a look at the generated [log files][2] or visit the
[gl pages][3] index directly.

[1]: https://$namespace.$gitlab/$repo/$mainfile.pdf
[2]: https://$namespace.$gitlab/$repo/$mainfile.log
[3]: https://$namespace.$gitlab/$repo/
