#!/bin/bash

set -e  # Exit with error code if subcommand does

# safety first
if [[ $(pwd) == *template* ]]
then
    echo "Don't run this in the template directory!"
    echo "Rename the current folder or edit init.sh to do this anyways"
    echo "The folder name must not contain the name 'template' or build will be aborted"
    exit 1
fi


# More safety second
echo "--------------------------------------------------------"
echo "THIS SCRIPT WILL NOW IRREVOCABLY DESTROY THIS TEMPLATE  "
echo "REPOSITORY AND REPLACE IT WITH A NEW ONE WITH YOUR GIVEN"
echo "PARAMETERS IN 'config'.                                 "
echo "ALL PRIOR DATA, INCLUDING BUT NOT LIMITED TO THE GIT    "
echo "HISTORY IN THIS DIRECTORY AND ALL ITS SUBDIRECTORIES    "
echo "WILL BE LOST.                                           "
echo "                                                        "
echo "YOUR GIVEN CONFIGURATION IS AS FOLLOWS:                 "
echo ""
cat config | tail -n 23
echo ""

read -p  "ARE YOU SURE YOU WANT TO CONTINUE? (y/n)   " -n 1 -r
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
  exit 1
fi
echo "" # Move to new line

{ ## DO NOT REMOVE THIS OR EVERYTHING BLOWS UP

# Read in config as environment variables
source config

# clean up meta files from template repository
rm init.sh config README.md LICENSE
rm -rf .git

# move template files into this directory
ls -a template | sed -r '/^\.+$/'d | xargs -I '{}' mv template/'{}' .
rmdir template

# Rename files
mv mainfile.tex $mainfile.tex
mv stylefile.sty $stylefile.sty

# replace config parameters
find -type f | xargs sed -i "s/\$mainfile/$(printf '%s\n' "$mainfile" | sed -e 's/[\/&]/\\&/g')/g"
find -type f | xargs sed -i "s/\$course/$(printf '%s\n' "$course" | sed -e 's/[\/&]/\\&/g')/g"
find -type f | xargs sed -i "s/\$stylefile/$(printf '%s\n' "$stylefile" | sed -e 's/[\/&]/\\&/g')/g"
find -type f | xargs sed -i "s/\$repo/$(printf '%s\n' "$repo" | sed -e 's/[\/&]/\\&/g')/g"
find -type f | xargs sed -i "s/\$namespace/$(printf '%s\n' "$namespace" | sed -e 's/[\/&]/\\&/g')/g"
find -type f | xargs sed -i "s/\$term/$(printf '%s\n' "$term" | sed -e 's/[\/&]/\\&/g')/g"
find -type f | xargs sed -i "s/\$lecturer/$(printf '%s\n' "$lecturer" | sed -e 's/[\/&]/\\&/g')/g"
find -type f | xargs sed -i "s/\$assistant/$(printf '%s\n' "$assistant" | sed -e 's/[\/&]/\\&/g')/g"
find -type f | xargs sed -i "s/\$author/$(printf '%s\n' "$author" | sed -e 's/[\/&]/\\&/g')/g"
find -type f | xargs sed -i "s/\$gitlab/$(printf '%s\n' "$gitlab" | sed -e 's/[\/&]/\\&/g')/g"

# init new repo
git init
git submodule add https://gitlab.com/latexci/packages/LatexPackagesBuild.git
git submodule foreach git checkout master-build
git add .
git commit -m "initial commit"

}
