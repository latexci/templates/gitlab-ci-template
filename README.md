# GitLab CI Template

## What you get

This template is intended to quickly set up a new LaTeX project with GitLab CI,
that is, automatic compilation of your LaTeX document.
Unfortunately, it will take some extra time on first usage to set up the GitLab runner,
but you only have to do this once.

The created project has the structure of the `template` folder present in this repository.
In particular, it is structured as follows:

- There is a main LaTeX file. This will include all other files, compiling this gives the desired output
- There is some auxiliary style file, custom for this project. The master file will include this and only this style file. Essentially, this is a preamble at some dedicated place
- Additionally, we set up the [LatexPackagesBuild][lpb] repository as a submodule to have custom packages available
- We use [latexmk][latexmk] to compile our document. A `.latexmkrc` is provided that makes `LaTeX`find the custom packages located in the submodule
- As another measure for dealing with the imports, there is a script file `export_texinputs.sh`. `source ./export_texinputs.sh` will set the `LaTeX` input path correctly, so that also `pdflatex` and friends will find the packages in the submodule.
- We provide a Makefile as an easy compile-interface. This also manages automatically initializing the submodule before compiling if it is not yet checked out. This ensures that `make` works in a fresh copy of the closed repository without explicitly having dealt with the submodule, so the work should be accessible for others easily.
- A standard `.gitignore` for LaTeX projects is provided.
- A standard `README.md` is provided, containing links to the CI.

## CI

The gitlab CI depends on the following assumptions:

- The repository will be hosted on a GitLab instance (not necessarily gitlab.com)
- You have a gitlab runner registered for your (new) project or any group that the project is a member of
- You enabled GitLab pages for your (new) project

It will then auto-compile the latest version of your document with each commit or tag and publish it at your GitLab pages environment so you (and possibly others) can directly download it without having to clone and build the repository themselves.

# Setup

## Template usage
To use this template:

- clone it to some dedicated place and give it the name of your new project:
`git clone https://gitlab.com/latexci/templates/gitlab-ci-template.git mynewproject`
- enter your data in the `config` file of the template (located in the root directory)
- execute `init.sh`

WARNING: execution of the `init.sh` shell script will IRREVOCABLY DESTROY the obtained copy of the template, including its git history, and set up your new project with an initial git commit. DO NOT run this `init.sh` if you modified the template and do not have a copy of it, or you risk losing your repository. The author takes no responsibility for such misusages.

For correct usages, this is of course the correct behaviour.

The data to be entered in the `config` file is as follows:

- `mainfile`: This is the name of your main `LaTeX` file, without extension
- `course`: A plaintext name of your course. Will be present in the `README.md` and set as title of the LaTeX file
- `stylefile`: The name of the custom style file / preamble, without extension
- `repo`: A url to the gitlab pages environment of your new project. This requires you to know the exact name of the GitLab repository you intend to host the new repository. It is given in one the following forms:  
`https://username.gitlab.io/projectname/`,  
 `https://groupname.gitlab.io/subgroup1/subgroup2/.../projectname/`  
To make the template work out correctly, the trailing slash is important.
- `term`: Some plain-text description of the term of the lecture notes.

## Enable gitlab runner
If you do not use a public runner (the author is not aware of any that are capable of handling LaTeX):

- If you did not already do so, or did for a whole group, now is the time to add your custom GitLab runner to your project.
Refer to [Register your runner](README.md#Register your runner).
- Explicitly disable the usage of public runners in your `Settings -> CI/CD -> Runners` section. Otherwise, your CI will fail and GitLab will ask you for a credit card to prevent misuse of public runners (which you don't use anyways, go blame GitLab for that antifeature).


## Enabling GitLab pages
Finally, do not forget to enable GitLab pages for your project (as far as the author is aware, this is only the default for public projects). Go to `Settings -> General`, expand the `Visibility, project features, permissions` tab, and activate the `Pages` switch (second from bottom). You can also set the access level there if you wish.

Refer to the [GitLab pages documentation][gl pages] for full details on how this works.

If you have not done so yet, refer to [setting up ci](README.md#Setting up GitLab CI) section to set up a GitLab runner.

# Example
An example project generated with the configuration

```
#!/bin/bash

mainfile='2022_Example_Course'
course='Example Course'
stylefile='example'
repo='https://latexci.gitlab.io/examples/gitlab-ci-example'
term='summer term 2022'
```
is available (without any further modification) as [gitlab-ci-example][gitlab-ci-example].

# Setting up GitLab CI
Refer to the [GitLab Runners Documentation][runnerdoc] for full details on how GitLab runners work. If you want to set up one yourself, for LaTeX projects, the author recommends the following:

### Set up docker container for compilation
- Set up your GitLab runner with the  `docker` executer. Then your runner will use a container for a controlled environment, refer to [GitLab docker installation][dockerrunner] for installation procedures.
- Install [docker][docker]
- Install some docker image that is able to build your project. For a default latex runner, the author recommends the [aergus/dockerfiles][aergus], it features a full TexLive installation and is availabe via DockerHub, so that your runner can automatically pull your image.
- If you also want to have python and the `tree` utility built into your image, you can use [kesslermaximilian/dockerfiles][kesslermaximilian], which is just a fork of the former adding this stuff.

For a custom docker image (not on DockerHub) only:
- Make sure you build your docker image and have it listed in the `docker images` output under the name you specified when setting up your
- You will have to edit the `config.toml` of your GitLab runner, since by default, it will try to pull from DockerHub. For this, add an entry `pull_policy = ["never"]` in the `[runners.docker]` section, this will make your runner use local docker images.

### Install the GitLab runner software
This is covered in the [documentation][runnerdoc] quite well. The author recommends also installing the runner in a container, you can then easily run this in the background, but of course, choose as you like.

### Register your runner
When registering your runner, it is important that you specify the `latex` tag (you will be prompted for a list of tags) so that GitLab will correctly find and use your runner.
Technically, you could also use some other tag, but this is the one the author chose for this template.
You will also have to provide the name of the docker container you intend to run your LaTeX in, so this will be (following above examples) `aergus/dockerfiles` or `kesslermaximilian/dockerfiles`.

# Windoof
As you might have noticed, The template script is written in bash and thus not (trivially) portable to Windows. The author does not care about that and sees little reason to do anything about it.

# Thanks
The author thanks Josia Pietsch for help with setting up and testing this template. The author also thanks StackOverflow, because he actually has no clue about `bash`.


[latexmk]: https://www.ctan.org/pkg/latexmk/
[lpb]: https://gitlab.com/latexci/packages/LatexPackagesBuild
[runnerdoc]: https://docs.gitlab.com/runner/
[dockerrunner]: https://docs.gitlab.com/runner/install/docker.html
[docker]: https://docs.docker.com/engine/install/centos/
[aergus]: https://gitlab.com/aergus/dockerfiles
[kesslermaximilian]: https://github.com/kesslermaximilian/dockerfiles
[gl pages]: https://docs.gitlab.com/ee/user/project/pages/
[gitlab-ci-example]: https://gitlab.com/latexci/examples/gitlab-ci-example
